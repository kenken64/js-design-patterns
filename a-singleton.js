/**
 * Created by phangty on 11/10/16.
 */
// Private
var x = require('./x');
var y = 'I am private';
var z = true;

function sum(num1, num2) {
    return num1 + num2;
}

// Public
var self = module.exports = {
    someProperty: 'I am public',
    addFive: function addFive(num) {
        return sum(num, 5);
    },

    toggleZ: function toggleZ(num) {
        console.log(num);
        console.log(z);
        return z = !z;
    }
};