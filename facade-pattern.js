/**
 * Created by phangty on 12/10/16.
 */
"use strict";

var Mortgage = function(name) {
    this.name = name;
}

Mortgage.prototype = {

    applyFor: function(amount) {
        // access multiple subsystems...
        var result = "approved";
        if (!new Bank().verify(this.name, amount)) {            // a Bank object will be created to help with checking
            result = "denied";
        } else if (!new Credit().get(this.name)) {              // a Credit object will be created to help with checking
            result = "denied";
        } else if (!new Background().check(this.name)) {            // a Background object will be created to help with checking
            result = "denied";
        }
        return this.name + " has been " + result +
            " for a " + amount + " mortgage";
    }
}

var Bank = function() {
    this.verify = function(name, amount) {
        // complex logic ...
        return true;
    }
}

var Credit = function() {
    this.get = function(name) {
        // complex logic ...
        return true;
    }
}

var Background = function() {
    this.check = function(name) {
        // complex logic ...
        return true;
    }
}

function run() {
    var mortgage = new Mortgage("Joan Templeton");
    var result = mortgage.applyFor("$100,000");

    console.log(result);
}

run();