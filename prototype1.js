/**
 * Created by phangty on 11/10/16.
 */
var TeslaModelS = function() {
    this.numWheels    = 4;
    this.manufacturer = 'Tesla';
    this.make         = 'Model S';
}

TeslaModelS.prototype = function() {

    var go = function() {
        // Rotate wheels
        console.log("Rotate wheels !");
    };

    var stop = function() {
        // Apply brake pads
        console.log("JAM brake !");
    };

    return {
        pressBrakePedal: stop,
        pressGasPedal: go
    }

}();

module.exports = TeslaModelS;