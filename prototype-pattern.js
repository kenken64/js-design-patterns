/**
 * Created by phangty on 11/10/16.
 */
// Import the file
var TeslaModelS = require('./prototype1');

// Create a variable for the prototype function
var x = new TeslaModelS();

// Use the prototype
x.pressBrakePedal();
x.pressGasPedal();