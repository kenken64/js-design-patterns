// Constructor pattern
// create new empty object
var newObject1 = { };
console.log(newObject1);

var newObject2 = { test: "1"};
console.log(newObject2.test);

var newObject3 = Object.create( Object.prototype );

console.log(newObject3);

var newObject4 = new Object();
console.log(newObject4);

newObject4.someKey = "Hello World";
console.log(newObject4.someKey);
newObject4.someNumber = 122222;
console.log(newObject4.someNumber);

var valueFromNewObject4 = newObject4.someKey;
console.log(valueFromNewObject4);

newObject4["someKey"] = "Hello World";
var valueWithSquareBracket = newObject4["someKey"];
console.log(valueWithSquareBracket);

var x = Object.defineProperty( newObject4, "someKey", {
    value: "for more control of the property's behavior",
    writable: false,
    enumerable: true,
    configurable: true
});

console.log(x.someKey);
x.someKey = "test 123";
console.log(x.someKey);

var o = {};
Object.defineProperty(o, 'a', { value: 1, enumerable: true });
Object.defineProperty(o, 'b', { value: 2, enumerable: false });
Object.defineProperty(o, 'c', { value: 3 }); // enumerable defaults to false
o.d = 4; // enumerable defaults to true when creating a property by setting it

for (var i in o) {
    console.log(i);
}
// logs 'a' and 'd' (in undefined order)

Object.keys(o); // ['a', 'd']

o.propertyIsEnumerable('a'); // true
o.propertyIsEnumerable('b'); // false
o.propertyIsEnumerable('c'); // false


var o = {};
Object.defineProperty(o, 'a', {
    get: function() { return 1; },
    configurable: false
});

console.log(o.a);

//Object.defineProperty(o, 'a', { value: 12 });

// To use, we then create a new empty "person" object
var person = Object.create( Object.prototype );

var defineProp = function ( obj, key, value ) {
    var config = {
        value: value,
        writable: true,
        enumerable: true,
        configurable: true
    };
    Object.defineProperty( obj, key, config );
};

// Populate the object with properties
defineProp( person, "car", "Toyota Camry" );
defineProp( person, "dateOfBirth", "1981" );
defineProp( person, "hasBeard", false );

console.log(person);
console.log(person["car"]);

function Car( model, year, miles ) {
    this.model = model;
    this.year = year;
    this.miles = miles;

    this.toString = function () {
        return this.model + " has done " + this.miles + " miles";
    };
}

var civic = new Car( "Honda Civic", 2009, 20000 );
var mondeo = new Car( "Ford Mondeo", 2010, 5000 );

console.log( civic.toString() );
console.log( mondeo.toString() );



