/**
 * Created by phangty on 12/10/16.
 */
var printer = (function () {
    var printerInstance;

    // Creating the instance
    function create () {
        function print() {
            // underlying printer mechanics
            console.log("print");
        }
        function turnOn() {
            // warm up
            // check for paper
            console.log("turn ON");
        }
        return {
            // public + private states and behaviours
            print: print,
            turnOn: turnOn
        };
    }

    // Return values for the function
    return {
        getInstance: function() {
            if(!printerInstance) {    // create one if there isn't already an instance
                printerInstance = create();
            }
            return printerInstance;
        }
    };


    function Singleton () {
        if(!printerInstance) {      // initialize if there isn't already an instance
            printerInstance = create();
        }
    };

})();

var singletonPrinter = printer.getInstance();
singletonPrinter.turnOn();
singletonPrinter.print();