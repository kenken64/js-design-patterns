/**
 * Created by phangty on 12/10/16.
 */
var myPromise = new Promise(function (resolve, reject) {
    setTimeout(function () {
        resolve("SOME VALUE");
    }, 2000);
});

myPromise
    .then(function () {
        console.log("Executes when accepted");
    })
    .catch(function () {
        console.log("Executes when rejected");
    });

function isValid(color) {
    return new Promise(function (resolve, reject) {
        if (color == "BLUE") {
            resolve("VALID COLOR");
        } else {
            resolve("INVALID COLOR");
        }
    });
}

isValid()
    .then(function (result) {
        console.log(result);
    })
    .catch(function (err) {
        console.log(err)
    });

var obj2 = {
    name: "John",
    say: function (prefix) {
        console.log(" say !");
        console.log(prefix);
        console.log(this.name);
        return prefix + this.name
    }
};
var anotherObj = {name: "Jack"}
obj2.say.call(anotherObj, "Hello2")


var obj3 = {
    name: "John",
    say: function (prefix, prefix2) {
        console.log(" say !");
        console.log(prefix);
        console.log(prefix2);
        console.log(this.name);
        return prefix + this.name;
    }
};
var anotherObj = {name: "Jack"}
obj3.say.apply(anotherObj, ["Hello ", "World"]);


