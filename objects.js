/**
 * Created by phangty on 12/10/16.
 */
var human = {};    //create a blank object
human.name = "John Doe";
human.sayHello = function () {
    console.log("Hello", human.name);
};
human.sayHello();
delete human.name //remove property
human.sayHello();


var human = {};    //create a blank object
human.name = "John Doe";

human.sayHello = function () {
    // notice use of this
    console.log("Hello", this.name);
};

human.sayHello();

function Person(name) {
    var job = "Engineer";//private property
    this.name = name;   //public property
};
var person = new Person("John Doe");
console.log(person.name); // John Doe
console.log(person.job); //undefined


function Person(name) {
    this.name = name;   //public property
};
var person = Person("John Doe");
console.log(typeof person); //
console.log(name); // John Doe


function Person2() {
}

var person = new Person2();
console.log(person instanceof Person2);
console.log(person instanceof Object);
console.log({} instanceof Object);
//console.log({} typeof person);
