/**
 * Created by phangty on 12/10/16.
 */
// Import the other file
var singleton = require('./a-singleton');

// Log the variables to see their results
//console.log(singleton.x);              // undefined (x is private)
//console.log(singleton.sum(1,2));       // undefined (sum is private)
console.log(singleton.someProperty);   // 'I am public'
console.log(singleton.toggleZ(1));      // false
console.log(singleton.toggleZ(2));      // true (public functions can still reference private variables)